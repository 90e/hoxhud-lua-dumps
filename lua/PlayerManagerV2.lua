clone_methods(PlayerManager)
function PlayerManager.on_used_body_bag(_ARG_0_, ...)
	_ARG_0_.oldMethods.on_used_body_bag(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_bodybags = _ARG_0_:get_body_bags_amount()
	})
end

function PlayerManager.add_body_bags_amount(_ARG_0_, ...)
	_ARG_0_.oldMethods.add_body_bags_amount(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_bodybags = _ARG_0_:get_body_bags_amount()
	})
end

function PlayerManager.activate_temporary_upgrade(_ARG_0_, ...)
	_ARG_0_.oldMethods.activate_temporary_upgrade(_ARG_0_, ...)
	managers.hud:activate_temp_upgrades_updator()
end

function PlayerManager.verify_carry(_ARG_0_, ...)
	return _ARG_0_._hoxhud_anticheat_temp_override and true or _ARG_0_.oldMethods.verify_carry(_ARG_0_, ...)
end

function PlayerManager.set_anticheat_override(_ARG_0_, _ARG_1_)
	_ARG_0_._hoxhud_anticheat_temp_override = _ARG_1_
end

function PlayerManager.peer_dropped_out(_ARG_0_, _ARG_1_, ...)
	UnitNetworkHandler:update_carry_bag(_ARG_1_:user_id(), nil)
	return _ARG_0_.oldMethods.peer_dropped_out(_ARG_0_, _ARG_1_, ...)
end

