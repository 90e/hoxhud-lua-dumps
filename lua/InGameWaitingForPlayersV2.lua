clone_methods(IngameWaitingForPlayersState)
LoadoutMusic = "HoxHud\\loadout_music_clipped.mp3"
function IngameWaitingForPlayersState.repeat_loadout_music()
	IngameWaitingForPlayersState._custom_music:stop()
	IngameWaitingForPlayersState._custom_music:queuefile(LoadoutMusic, 84900)
	IngameWaitingForPlayersState._custom_music:setcallback(IngameWaitingForPlayersState.repeat_loadout_music)
	IngameWaitingForPlayersState._custom_music:play()
	IngameWaitingForPlayersState._custom_music:set_volume(managers.user:get_setting("music_volume") or Global.music_manager.volume and Global.music_manager.volume * 100 or 1)
end

function IngameWaitingForPlayersState.at_enter(_ARG_0_, ...)
	_ARG_0_.oldMethods.at_enter(_ARG_0_, ...)
	if not io.open(LoadoutMusic, "r") or not PlayMediaV2 then
		return
	end

	io.open(LoadoutMusic, "r"):close()
	managers.music:stop()
	IngameWaitingForPlayersState._custom_music = PlayMediaV2(LoadoutMusic)
	_ARG_0_._custom_music:setcallback(_ARG_0_.repeat_loadout_music)
	_ARG_0_._custom_music:play()
	_ARG_0_._custom_music:set_volume(managers.user:get_setting("music_volume") or Global.music_manager.volume and Global.music_manager.volume * 100 or 1)
end

function IngameWaitingForPlayersState.sync_start(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_start(_ARG_0_, ...)
	if _ARG_0_._custom_music then
		_ARG_0_._custom_music:stop()
		_ARG_0_._custom_music = nil
	end
end

function IngameWaitingForPlayersState.at_exit(_ARG_0_, ...)
	_ARG_0_.oldMethods.at_exit(_ARG_0_, ...)
	if _ARG_0_._custom_music then
		_ARG_0_._custom_music:stop()
		_ARG_0_._custom_music = nil
	end
end

