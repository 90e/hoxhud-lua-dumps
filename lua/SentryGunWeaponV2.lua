clone_methods(SentryGunWeapon)
function SentryGunWeapon.setup(_ARG_0_, ...)
	_ARG_0_.oldMethods.setup(_ARG_0_, ...)
	managers.hud:add_sentry_unit(_ARG_0_._unit)
	if Network:is_server() then
		managers.hud:update_sentry_unit(_ARG_0_._unit, _ARG_0_:ammo_max(), _ARG_0_:ammo_max())
	else
		managers.hud:update_sentry_unit(_ARG_0_._unit, 100, 100)
	end
end

function SentryGunWeapon.sync_ammo(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_ammo(_ARG_0_, ...)
	if _ARG_0_:ammo_ratio() <= 0 then
		managers.hud:del_sentry_unit(_ARG_0_._unit)
	else
		managers.hud:update_sentry_unit(_ARG_0_._unit, _ARG_0_._ammo_ratio, 100)
	end
end

function SentryGunWeapon.stop_autofire(_ARG_0_, ...)
	_ARG_0_.oldMethods.stop_autofire(_ARG_0_, ...)
	if _ARG_0_:out_of_ammo() then
		managers.hud:del_sentry_unit(_ARG_0_._unit)
	end
end

function SentryGunWeapon.fire(_ARG_0_, ...)
	managers.hud:update_sentry_unit(_ARG_0_._unit, _ARG_0_._ammo_total, _ARG_0_._ammo_max)
	return (_ARG_0_.oldMethods.fire(_ARG_0_, ...))
end

