clone_methods(TweakData)

if type(nil) == "function" then
	HoxHudTweakData[nil] = function(...)
		setfenv(1, {
			string = _UPVALUE0_(string),
			math = _UPVALUE0_(math),
			Vector3 = _UPVALUE0_(Vector3),
			Rotation = _UPVALUE0_(Rotation),
			Color = _UPVALUE0_(Color),
			setfenv = setfenv
		})
		return _UPVALUE1_(...)
	end
end

function TweakData.init(_ARG_0_, ...)
	TweakData.oldMethods.init(_ARG_0_, ...)
	_ARG_0_.hoxhud = HoxHudTweakData:new()
	
	if type(....color) == "string" and not Color[....name] then
		Color[....name] = Color(....color)
		_ARG_0_.hoxhud.local_strings["menu_color_" .. ....name] = tostring(....nice_name)
	end
	
	_ARG_0_.hoxhud.local_strings[...] = _ARG_0_.hoxhud.local_strings
end

tweak_data = TweakData:new()
