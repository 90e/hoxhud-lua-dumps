clone_methods(SecurityCamera)
function SecurityCamera.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_0_._tape_loop_timer and not _ARG_0_._suspicion then
		if _ARG_0_._tape_loop_end_t then
			_ARG_0_._tape_loop_total = _ARG_0_._tape_loop_total or _ARG_0_._tape_loop_end_t - _ARG_2_
			_ARG_0_._tape_loop_timer:set_completion(_ARG_0_._tape_loop_end_t - _ARG_2_, _ARG_0_._tape_loop_total)
		elseif _ARG_0_._tape_loop_timer and _ARG_0_._tape_loop_restarting_t then
			_ARG_0_._tape_loop_total = _ARG_0_._tape_loop_total or _ARG_0_._tape_loop_restarting_t - _ARG_2_
			_ARG_0_._tape_loop_timer:set_completion(_ARG_0_._tape_loop_restarting_t - _ARG_2_, _ARG_0_._tape_loop_total)
		end
	end

	return _ARG_0_.oldMethods.update(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
end

function SecurityCamera._upd_detection(_ARG_0_, ...)
	if _ARG_0_._tape_loop_timer then
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		_ARG_0_._tape_loop_timer = nil
	end

	return _ARG_0_.oldMethods._upd_detection(_ARG_0_, ...)
end

function SecurityCamera.set_detection_enabled(_ARG_0_, _ARG_1_, ...)
	_ARG_0_._camera_on = _ARG_1_
	if not _ARG_1_ and _ARG_0_._tape_loop_timer then
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		_ARG_0_._tape_loop_timer = nil
	end

	return _ARG_0_.oldMethods.set_detection_enabled(_ARG_0_, _ARG_1_, ...)
end

function SecurityCamera.can_apply_tape_loop(_ARG_0_, ...)
	if Network:is_server() and not _ARG_0_._camera_on then
		return false
	end

	return _ARG_0_.oldMethods.can_apply_tape_loop(_ARG_0_, ...)
end

function SecurityCamera._start_tape_loop_hud_timer(_ARG_0_)
	if not _ARG_0_._alarm_sound and (not Network:is_server() or _ARG_0_._camera_on) then
		if _ARG_0_._tape_loop_timer then
			managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		end

		_ARG_0_._tape_loop_timer = managers.hud:add_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, tweak_data.hoxhud.tape_loop_name, {
			tweak_name = "tape_loop",
			complete_color = tweak_data.hoxhud.tape_loop_expire_color,
			flash_period = tweak_data.hoxhud.tape_loop_restart_flash_period
		})
	end
end

function SecurityCamera._start_tape_loop_by_upgrade_level(_ARG_0_, ...)
	_ARG_0_:_start_tape_loop_hud_timer()
	return _ARG_0_.oldMethods._start_tape_loop_by_upgrade_level(_ARG_0_, ...)
end

function SecurityCamera._request_start_tape_loop_by_upgrade_level(_ARG_0_, ...)
	if Network:is_server() and not _ARG_0_._camera_on then
		return
	end

	_ARG_0_:_start_tape_loop_hud_timer()
	return _ARG_0_.oldMethods._request_start_tape_loop_by_upgrade_level(_ARG_0_, ...)
end

function SecurityCamera._clbk_tape_loop_expired(_ARG_0_, ...)
	_ARG_0_.oldMethods._clbk_tape_loop_expired(_ARG_0_, ...)
	_ARG_0_._tape_loop_total = nil
	if _ARG_0_._tape_loop_timer then
		if not managers.groupai:state():whisper_mode() then
			managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
			_ARG_0_._tape_loop_timer = nil
		else
			_ARG_0_._tape_loop_timer:set_jammed(true)
		end
	end
end

function SecurityCamera.sync_net_event(_ARG_0_, _ARG_1_)
	if not Network:is_server() then
		if _ARG_1_ == _ARG_0_._NET_EVENTS.start_tape_loop_1 or _ARG_1_ == _ARG_0_._NET_EVENTS.start_tape_loop_2 then
			_ARG_0_._tape_loop_end_t = TimerManager:game():time() + tweak_data.upgrades.values.player.tape_loop_duration[_ARG_1_ - (_ARG_0_._NET_EVENTS.start_tape_loop_1 - 1)]
			_ARG_0_:set_update_enabled(true)
		elseif _ARG_1_ == _ARG_0_._NET_EVENTS.request_start_tape_loop_1 or _ARG_1_ == _ARG_0_._NET_EVENTS.request_start_tape_loop_2 then
			_ARG_0_._tape_loop_end_t = TimerManager:game():time() + tweak_data.upgrades.values.player.tape_loop_duration[_ARG_1_ - (_ARG_0_._NET_EVENTS.request_start_tape_loop_1 - 1)]
			_ARG_0_:set_update_enabled(true)
		end
	end

	return _ARG_0_.oldMethods.sync_net_event(_ARG_0_, _ARG_1_)
end

function SecurityCamera.generate_cooldown(_ARG_0_, ...)
	if _ARG_0_._tape_loop_timer then
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		_ARG_0_._tape_loop_timer = nil
	end

	return _ARG_0_.oldMethods.generate_cooldown(_ARG_0_, ...)
end

function SecurityCamera.set_update_enabled(_ARG_0_, _ARG_1_, ...)
	if not _ARG_1_ and _ARG_0_._tape_loop_timer and (not Network:is_server() or Network:is_server() and (not _ARG_1_ or not managers.groupai:state():whisper_mode())) then
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		_ARG_0_._tape_loop_timer = nil
	end

	return _ARG_0_.oldMethods.set_update_enabled(_ARG_0_, _ARG_1_, ...)
end

function SecurityCamera.destroy(_ARG_0_, ...)
	if _ARG_0_._tape_loop_timer then
		managers.hud:del_hud_timer(tweak_data.hoxhud.tab_screen_timers.tape_loop, _ARG_0_._tape_loop_timer)
		_ARG_0_._tape_loop_timer = nil
	end

	return _ARG_0_.oldMethods.destroy(_ARG_0_, ...)
end

