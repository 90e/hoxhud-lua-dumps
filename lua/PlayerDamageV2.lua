clone_methods(PlayerDamage)
function PlayerDamage.update(_ARG_0_, ...)
	_ARG_0_.oldMethods.update(_ARG_0_, ...)
	if not not managers.user:get_setting("hoxhud_show_armor_regen_timer") then
		managers.hud:update_armor_regen(_ARG_0_._regenerate_timer or 0)
	end
end

function PlayerDamage._hit_direction(_ARG_0_, _ARG_1_, ...)
	if not _ARG_1_ then
		return
	end

	_ARG_0_.oldMethods._hit_direction(_ARG_0_, _ARG_1_, ...)
	managers.hud:on_hit_direction(_ARG_1_.ray, _ARG_0_._unit:camera())
end

