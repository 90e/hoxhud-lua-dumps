clone_methods(MissionEndState)
function MissionEndState.setup_controller(_ARG_0_, ...)
	if not tweak_data.hoxhud.debug_enable_skip_endscreen then
		return _ARG_0_.oldMethods.setup_controller(_ARG_0_, ...)
	end

	_ARG_0_.oldMethods.setup_controller(_ARG_0_, ...)
	_ARG_0_._completion_bonus_done = true
	_ARG_0_._continue_block_timer = nil
end

function MissionEndState._continue_blocked(_ARG_0_, ...)
	if not tweak_data.hoxhud.debug_enable_skip_endscreen then
		return _ARG_0_.oldMethods._continue_blocked(_ARG_0_, ...)
	end

	return false
end

function MissionEndState.completion_bonus_done(_ARG_0_, ...)
	if not tweak_data.hoxhud.debug_enable_skip_endscreen then
		return _ARG_0_.oldMethods.completion_bonus_done(_ARG_0_, ...)
	end

	_ARG_0_.oldMethods.completion_bonus_done(_ARG_0_, ...)
	_ARG_0_._completion_bonus_done = true
end

