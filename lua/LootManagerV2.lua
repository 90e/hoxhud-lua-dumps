clone_methods(LootManager)
function LootManager.sync_load(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_load(_ARG_0_, ...)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._global.secured) do
		_FORV_7_.multiplier = (_FORV_7_.multiplier > tweak_data.upgrades.values.player.small_loot_multiplier[#tweak_data.upgrades.values.player.small_loot_multiplier] or _FORV_7_.multiplier < 1) and 1 or _FORV_7_.multiplier
	end
end

function LootManager.would_be_bonus_bag(_ARG_0_, _ARG_1_)
	if (_ARG_0_._global.mandatory_bags.amount or 0) == 0 then
		return true
	end
	
	if not tweak_data.carry.small_loot[nil.carry_id] then
		if 0 < (_ARG_0_._global.mandatory_bags.amount or 0) and (_ARG_0_._global.mandatory_bags.carry_id == "none" or _ARG_0_._global.mandatory_bags.carry_id == nil.carry_id) then
		elseif (_ARG_0_._global.mandatory_bags.amount or 0) - 1 == 0 then
			return true
		end
	end

	return (_ARG_0_._global.mandatory_bags.amount or 0) - 1 == 0
end

