core:import("UserManager")
do break end
if type("black".name) == "string" then
	do break end
	if nil == "black".name then
	else
	end

	if not (nil == "black".name) then
		table.insert({
			"red",
			"green",
			"blue",
			"yellow",
			"black",
			"white",
			"purple"
		}, "black".name)
	end
end

do break end
table.insert({}, {
	localize = true,
	text_id = "menu_color_" .. "black",
	value = "black"
})
do break end
table.insert({}, {
	localize = true,
	text_id = "menu_pti_" .. "pagers_and_locks",
	value = "pagers_and_locks"
})
UserManager.GenericUserManager.HOXHUD_START_NUM = 100
UserManager.GenericUserManager.HOXHUD_SETTINGS = {
	{
		name = "hoxhud_main_options",
		next_node = "hoxhud_main_options",
		_meta = "item",
		text_id = "menu_hoxhud_main_options",
		help_id = "menu_hoxhud_main_options_help",
		items = {
			{
				[0] = 100,
				"hoxhud_anticheat_only",
				false
			},
			{
				[0] = 101,
				"hoxhud_hud_only",
				false
			},
			{
				[0] = 102,
				"hoxhud_show_interact_circle",
				false
			},
			{
				[0] = 103,
				"hoxhud_show_interact_timer",
				true
			},
			{
				[0] = 104,
				"hoxhud_show_timer_on_reload",
				true
			},
			{
				[0] = 105,
				"hoxhud_recolor_name_text",
				true
			},
			{
				[0] = 106,
				"hoxhud_show_armor_regen_timer",
				true
			},
			{
				[0] = 107,
				"hoxhud_show_kill_counter",
				true
			},
			{
				[0] = 108,
				"hoxhud_show_numeric_suspicion",
				true
			},
			{
				[0] = 109,
				"hoxhud_kill_counter_color",
				"orange",
				{}
			},
			{
				[0] = 110,
				"hoxhud_armor_regen_color",
				"white",
				{}
			},
			{
				[0] = 111,
				"hoxhud_undrdg_health_flash",
				"yellow",
				{}
			},
			{
				[0] = 112,
				"hoxhud_inf_ammo_flash_col",
				"light_blue",
				{}
			},
			{
				[0] = 113,
				"hoxhud_wpn_laser_color",
				"green",
				{}
			},
			{
				[0] = 114,
				"hoxhud_interact_timer_color",
				"green",
				{}
			},
			{
				[0] = 115,
				"hoxhud_armor_regen_size",
				22,
				{
					16,
					28,
					1
				}
			},
			{
				[0] = 116,
				"hoxhud_press_to_interact",
				"all_5s",
				{}
			},
			{
				[0] = 162,
				"hoxhud_pti_threshold",
				5,
				{
					1,
					15,
					0.1
				}
			}
		}
	},
	{
		name = "hoxhud_extra_options",
		next_node = "hoxhud_extra_options",
		_meta = "item",
		text_id = "menu_hoxhud_extra_options",
		help_id = "menu_hoxhud_extra_options_help",
		items = {
			{
				[0] = 117,
				"hoxhud_show_timer_panel",
				true
			},
			{
				[0] = 118,
				"hoxhud_show_pager_timers",
				true
			},
			{
				[0] = 119,
				"hoxhud_show_pagers_as_client",
				true
			},
			{
				[0] = 120,
				"hoxhud_sense_of_humour_bypass",
				false
			},
			{
				[0] = 121,
				"hoxhud_disable_orig_suspicion",
				false
			},
			{
				[0] = 122,
				"hoxhud_rotate_for_angled_sight",
				true
			},
			{
				[0] = 123,
				"hoxhud_numeric_susp_min_col",
				"baby_blue",
				{}
			},
			{
				[0] = 124,
				"hoxhud_numeric_susp_max_col",
				"orange_red",
				{}
			},
			{
				[0] = 125,
				"hoxhud_susp_back_alpha",
				0.5,
				{
					0,
					1,
					0.01
				}
			},
			{
				[0] = 126,
				"hoxhud_civ_pacified_icon_col",
				"green",
				{}
			},
			{
				[0] = 127,
				"hoxhud_civ_pacified_arrow_col",
				"pastel_green",
				{}
			},
			{
				[0] = 128,
				"hoxhud_hit_ind_fade_duration",
				2.4,
				{
					0,
					5,
					0.1
				}
			},
			{
				[0] = 129,
				"hoxhud_hit_ind_box_diameter",
				384,
				{
					150,
					500,
					1
				}
			},
			{
				[0] = 130,
				"hoxhud_hit_ind_start_col",
				"white",
				{}
			},
			{
				[0] = 131,
				"hoxhud_hit_ind_fade_col",
				"red",
				{}
			},
			{
				[0] = 132,
				"hoxhud_hit_ind_max_num",
				6,
				{
					1,
					20,
					1
				}
			},
			{
				[0] = 163,
				"hoxhud_headshot_gore",
				true
			},
			{
				[0] = 164,
				"hoxhud_tabscreen_migrate",
				60,
				{
					0,
					500,
					1
				}
			}
		}
	},
	{
		name = "hoxhud_enemy_health_options",
		next_node = "hoxhud_enemy_health_options",
		_meta = "item",
		text_id = "menu_hoxhud_enemy_health_options",
		help_id = "menu_hoxhud_enemy_health_options_help",
		items = {
			{
				[0] = 133,
				"hoxhud_show_enemy_health",
				true
			},
			{
				[0] = 134,
				"hoxhud_show_enemy_health_mul",
				true
			},
			{
				[0] = 135,
				"hoxhud_show_civilian_health",
				false
			},
			{
				[0] = 136,
				"hoxhud_enemy_health_size",
				100,
				{
					50,
					200,
					1
				}
			},
			{
				[0] = 137,
				"hoxhud_enemy_health_vert",
				110,
				{
					0,
					300,
					1
				}
			},
			{
				[0] = 138,
				"hoxhud_enemy_health_horz",
				0,
				{
					-400,
					400,
					1
				}
			},
			{
				[0] = 139,
				"hoxhud_enemy_hurt_color",
				"orange",
				{}
			},
			{
				[0] = 140,
				"hoxhud_enemy_kill_color",
				"red",
				{}
			},
			{
				[0] = 141,
				"hoxhud_undrdog_dmg_boost_col",
				"light_yellow",
				{}
			},
			{
				[0] = 142,
				"hoxhud_overkill_dmg_boost_col",
				"red",
				{}
			}
		}
	},
	{
		name = "hoxhud_enemy_dmg_options",
		next_node = "hoxhud_enemy_dmg_options",
		_meta = "item",
		text_id = "menu_hoxhud_enemy_dmg_options",
		help_id = "menu_hoxhud_enemy_dmg_options_help",
		items = {
			{
				[0] = 143,
				"hoxhud_show_dmg_counter",
				true
			},
			{
				[0] = 144,
				"hoxhud_show_dmg_per_hit",
				false
			},
			{
				[0] = 145,
				"hoxhud_dmg_counter_ignore_civs",
				false
			},
			{
				[0] = 146,
				"hoxhud_hit_chg_per_tick",
				0.1,
				{
					0,
					2,
					0.1
				}
			},
			{
				[0] = 147,
				"hoxhud_fatal_hit_chg_per_tick",
				0.5,
				{
					0,
					2,
					0.1
				}
			},
			{
				[0] = 148,
				"hoxhud_hit_display_duration",
				3.5,
				{
					0,
					7,
					0.1
				}
			},
			{
				[0] = 149,
				"hoxhud_dmg_vert_offset",
				60,
				{
					0,
					300,
					1
				}
			},
			{
				[0] = 150,
				"hoxhud_dmg_kill_flash_spd",
				4,
				{
					0,
					10,
					1
				}
			},
			{
				[0] = 151,
				"hoxhud_dmg_hit_color",
				"white",
				{}
			},
			{
				[0] = 152,
				"hoxhud_dmg_kill_color",
				"orange",
				{}
			},
			{
				[0] = 153,
				"hoxhud_headshot_flash_color",
				"red",
				{}
			}
		}
	},
	{
		name = "hoxhud_infobox_options",
		next_node = "hoxhud_infobox_options",
		_meta = "item",
		text_id = "menu_hoxhud_infobox_options",
		help_id = "menu_hoxhud_infobox_options_help",
		items = {
			{
				[0] = 154,
				"hoxhud_pagers_infobox_enable",
				true
			},
			{
				[0] = 155,
				"hoxhud_alrtcivs_infobox_enable",
				true
			},
			{
				[0] = 156,
				"hoxhud_dom_infobox_enable",
				true
			},
			{
				[0] = 157,
				"hoxhud_jkr_infobox_enable",
				true
			},
			{
				[0] = 158,
				"hoxhud_bdy_infobox_enable",
				true
			},
			{
				[0] = 159,
				"hoxhud_sentry_infobox_enable",
				true
			},
			{
				[0] = 160,
				"hoxhud_ecmfb_infobox_enable",
				true
			},
			{
				[0] = 161,
				"hoxhud_gage_infobox_enable",
				true
			}
		}
	}
}
function UserManager.GenericUserManager.nuke_hoxhud_settings(_ARG_0_)
	do break end
	if nil >= _ARG_0_.HOXHUD_START_NUM then
		Global.user_manager.setting_map[nil] = nil
	end

	managers.savefile:save_game(managers.savefile.SETTING_SLOT, false)
	managers.savefile:save_game(managers.savefile.SETTING_SLOT, true)
end

