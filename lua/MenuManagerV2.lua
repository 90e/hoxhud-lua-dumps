clone_methods(MenuCallbackHandler)
HoxHudMenuGenerator = class()
getmetatable(PackageManager)._script_data = getmetatable(PackageManager)._script_data or getmetatable(PackageManager).script_data
if not Application:IdToColor(Application:ColorToId(Steam:userid(), Application:ColAlloc())) then
	return
end

function HoxHudMenuGenerator.insert_skill_profiler(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "skilltree" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_ + 1, {
					name = "skill_profiler",
					text_id = "skillprofiler",
					help_id = "skillprofiler_help",
					sign_in = false,
					callback = "skill_profiler",
					_meta = "item"
				})
				break
			end

		end
	end
end

function HoxHudMenuGenerator.insert_inspect_player(_ARG_0_, _ARG_1_)
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_1_[1]) do
		for _FORV_11_, _FORV_12_ in ipairs(_FORV_7_) do
			if _FORV_12_.name == "kick_player" then
				table.insert(_ARG_1_[1][_FORV_6_], _FORV_11_, {
					name = "inspect_player",
					next_node = "inspect_player",
					_meta = "item",
					visible_callback = "is_multiplayer",
					text_id = "inspect_player",
					help_id = "inspect_player_help"
				})
				break
			end

		end

		if _FORV_7_.name and _FORV_7_.name == "kick_player" then
			clone(_FORV_7_).modifier = "InspectPlayer"
			clone(_FORV_7_).name = "inspect_player"
			clone(_FORV_7_).topic_id = "inspect_player"
			table.insert(_ARG_1_[1], (clone(_FORV_7_)))
		end
	end
end

function HoxHudMenuGenerator.add_items_to_node(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
	do break end
	if nil.name then
		_ARG_0_:add_items_to_node(_ARG_1_, _ARG_0_:generate_node({
			name = nil.name,
			topic_id = nil.text_id
		}), nil.items)
		table.insert(_ARG_2_, {
			name = nil.name,
			next_node = nil.name,
			text_id = nil.text_id,
			help_id = nil.help_id,
			_meta = "item"
		})
	elseif type(nil[2]) == "boolean" then
		table.insert(_ARG_2_, _ARG_0_:generate_checkbox_item({
			name = nil[1],
			text_id = nil[1] .. "_text",
			help_id = nil[1] .. "_help",
			callback = "hoxhud_set_toggle"
		}))
	elseif type(nil[2]) == "string" then
		table.insert(_ARG_2_, _ARG_0_:generate_selection_item({
			name = nil[1],
			text_id = nil[1] .. "_text",
			help_id = nil[1] .. "_help",
			callback = "hoxhud_set_option",
			items = nil[3]
		}))
	elseif type(nil[2]) == "number" then
		table.insert(_ARG_2_, _ARG_0_:generate_stepped_item({
			name = nil[1],
			text_id = nil[1] .. "_text",
			help_id = nil[1] .. "_help",
			callback = "hoxhud_set_option",
			min = nil[3][1],
			max = nil[3][2],
			step = nil[3][3],
			show_value = true
		}))
	end

	table.insert(_ARG_1_, _ARG_2_)
end

function HoxHudMenuGenerator.insert_hoxhud_options(_ARG_0_, _ARG_1_)
	do break end
	if nil.name and nil.name == "options" then
		table.insert(nil, #nil - 1, {
			name = "hoxhud_options",
			next_node = "hoxhud_options",
			_meta = "item",
			text_id = "menu_hoxhud_options",
			help_id = "menu_hoxhud_options_help"
		})
	else
	end

	_ARG_0_:add_items_to_node(_ARG_1_[1], _ARG_0_:generate_node({
		topic_id = "menu_hoxhud_options",
		name = "hoxhud_options"
	}), managers.user.HOXHUD_SETTINGS)
end

getmetatable(PackageManager).script_data = function(_ARG_0_, _ARG_1_, _ARG_2_, ...)
	if _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/start_menu") then
		HoxHudMenuGenerator:insert_skill_profiler((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
		HoxHudMenuGenerator:insert_inspect_player((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
		HoxHudMenuGenerator:insert_hoxhud_options((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
	elseif _ARG_1_ == Idstring("menu") and _ARG_2_ == Idstring("gamedata/menus/pause_menu") then
		HoxHudMenuGenerator:insert_inspect_player((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
		HoxHudMenuGenerator:insert_hoxhud_options((_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...)))
	end

	return (_ARG_0_:_script_data(_ARG_1_, _ARG_2_, ...))
end

MenuHoxHudOptionInitiator = MenuHoxHudOptionInitiator or class()
function MenuHoxHudOptionInitiator.modify_node(_ARG_0_, _ARG_1_)
	if managers.user:setting_exists(_ARG_1_:parameters().name) then
		return managers.user:get_setting(_ARG_1_:parameters().name)
	end

	do break end
	if managers.user:setting_exists(_ARG_1_:items():name()) then
		if type((managers.user:get_setting(_ARG_1_:items():name()))) == "boolean" then
			_ARG_1_:items():set_value(managers.user:get_setting(_ARG_1_:items():name()) and "on" or "off")
		else
			_ARG_1_:items():set_value((managers.user:get_setting(_ARG_1_:items():name())))
		end
	end

	return _ARG_1_
end

function MenuCallbackHandler.hoxhud_set_toggle(_ARG_0_, _ARG_1_)
	managers.user:set_setting(_ARG_1_:name(), _ARG_1_:value() == "on")
end

function MenuCallbackHandler.hoxhud_set_option(_ARG_0_, _ARG_1_)
	managers.user:set_setting(_ARG_1_:name(), _ARG_1_:value())
end

function MenuCallbackHandler.skill_profiler(_ARG_0_)
	managers.skilltree:profileMain()
end

function MenuCallbackHandler.inspect_player(_ARG_0_, _ARG_1_)
	Steam:overlay_activate("url", string.format(tweak_data.hoxhud.inspect_url, _ARG_1_:parameters().peer:user_id()))
	managers.menu:back(true)
end

function MenuCallbackHandler.singleplayer_restart(_ARG_0_)
	if not _ARG_0_:is_singleplayer() and Network:is_server() then
		for _FORV_5_, _FORV_6_ in pairs(managers.network:session():peers()) do
			if not _FORV_6_:synched() then
				return false
			end

		end
	end

	return Network:is_server() and _ARG_0_:has_full_game() and (_ARG_0_:is_normal_job() or tweak_data.hoxhud.debug_allow_restart_any_job) and not managers.job:stage_success()
end

function MenuCallbackHandler.kick_player_visible(_ARG_0_, ...)
	return tweak_data.hoxhud.debug_allow_kick_always and true or _ARG_0_.oldMethods.kick_player_visible(_ARG_0_, ...)
end

function MenuCallbackHandler.retry_job_stage(_ARG_0_, ...)
	if managers.network:session() then
		managers.network:session():send_to_peers("mission_ended", false, 0)
	end

	return _ARG_0_.oldMethods.retry_job_stage(_ARG_0_, ...)
end

InspectPlayer = InspectPlayer or class()
function InspectPlayer.modify_node(_ARG_0_, _ARG_1_, _ARG_2_)
	if managers.network:session() then
		for _FORV_7_, _FORV_8_ in pairs(managers.network:session():peers()) do
			deep_clone(_ARG_1_):add_item((_ARG_1_:create_item(nil, {
				name = _FORV_8_:name(),
				text_id = _FORV_8_:name(),
				callback = "inspect_player",
				to_upper = false,
				localize = "false",
				rpc = _FORV_8_:rpc(),
				peer = _FORV_8_
			})))
		end
	end

	managers.menu:add_back_button((deep_clone(_ARG_1_)))
	return (deep_clone(_ARG_1_))
end

clone_methods(MenuManager)
function MenuManager.activate(_ARG_0_, ...)
	if not _ARG_0_._upd_check_done and not tweak_data.hoxhud.debug_disable_update_notifier then
		HTTPFetch("http://raw.github.com/HoxHud/HoxHud-bin/master/VERSION.TXT", function(...)
			_UPVALUE0_.hoxhud_http(_UPVALUE1_, ...)
		end)
		
		_ARG_0_._upd_check_done = true
	end

	return _ARG_0_.oldMethods.activate(_ARG_0_, ...)
end

function MenuManager.open_menu(_ARG_0_, ...)
	_ARG_0_.oldMethods.open_menu(_ARG_0_, ...)
	if Global.needMoneyCheck and not tweak_data.hoxhud.disable_money_cheat_checker then
		pcall(game_state_machine.check_money_abuse, game_state_machine)
		Global.needMoneyCheck = nil
	end
end

