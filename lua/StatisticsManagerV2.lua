clone_methods(StatisticsManager)
function StatisticsManager.killed(_ARG_0_, _ARG_1_)
	if _ARG_1_.name:find("civilian") then
		_ARG_1_.head_shot = false
	end

	_ARG_0_.oldMethods.killed(_ARG_0_, _ARG_1_)
	if (_ARG_1_.weapon_unit and _ARG_1_.weapon_unit:base():get_name_id() or "") == "sentry_gun" then
		_ARG_0_._sentry_headshots = (_ARG_0_._sentry_headshots or 0) + (_ARG_1_.head_shot and 1 or 0)
		_ARG_0_._sentry_kills = (_ARG_0_._sentry_kills or 0) + 1
	end

	managers.hud:update_kill_counter(HUDManager.PLAYER_PANEL, _ARG_0_:session_total_head_shots(), _ARG_0_:session_total_kills() - _ARG_0_:session_total_civilian_kills() - _ARG_0_:session_total_head_shots(), _ARG_0_._sentry_headshots, _ARG_0_._sentry_kills, deep_clone(_ARG_0_:get_session_killed()))
end

function StatisticsManager.in_custody(_ARG_0_, ...)
	managers.hud:hide_interaction_bar(false)
	managers.hud:set_enemy_health_visible(false)
	return _ARG_0_.oldMethods.in_custody(_ARG_0_, ...)
end

function StatisticsManager.get_session_killed(_ARG_0_)
	return _ARG_0_._global.session.killed
end

function StatisticsManager.get_session_melee_kills(_ARG_0_)
	return _ARG_0_._global.session.killed.total.melee
end

