function MoneyManager.get_unsecured_bonus_bag_value(_ARG_0_, _ARG_1_, _ARG_2_)
	if not managers.loot:would_be_bonus_bag(_ARG_1_) then
		return 0
	end

	return math.round((_ARG_0_:get_bag_value(_ARG_1_, _ARG_2_) + math.round(_ARG_0_:get_bag_value(_ARG_1_, _ARG_2_) * (managers.job:current_job_id() and #tweak_data.narrative.jobs[managers.job:current_job_id()].chain or 1) * _ARG_0_:get_contract_difficulty_multiplier(managers.job:has_active_job() and managers.job:current_difficulty_stars() or 0))) * managers.player:upgrade_value("player", "secured_bags_money_multiplier", 1) / _ARG_0_:get_tweak_value("money_manager", "offshore_rate"))
end

