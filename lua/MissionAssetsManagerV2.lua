clone_methods(MissionAssetsManager)
if Global.job_manager and Global.job_manager.current_job and Global.job_manager.current_job.job_id == "big" then
	return
end

function MissionAssetsManager._setup_mission_assets(_ARG_0_, ...)
	_ARG_0_.oldMethods._setup_mission_assets(_ARG_0_, ...)
	_ARG_0_:insert_buy_all_assets_asset()
end

function MissionAssetsManager.update_buy_all_assets_asset_cost(_ARG_0_)
	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._global.assets) do
		if _FORV_6_.id ~= "buy_all_assets" and _FORV_6_.show and not _FORV_6_.unlocked and (not Network:is_server() or not _FORV_6_.can_unlock) and Network:is_client() and _ARG_0_:get_asset_can_unlock_by_id(_FORV_6_.id) then
		end
	end

	_ARG_0_._tweak_data.buy_all_assets.money_lock = 0 + (_ARG_0_._tweak_data[_FORV_6_.id].money_lock or 0)
end

function MissionAssetsManager.insert_buy_all_assets_asset(_ARG_0_)
	if not _ARG_0_._tweak_data.gage_assignment then
		return
	end

	_ARG_0_._tweak_data.buy_all_assets = clone(_ARG_0_._tweak_data.gage_assignment)
	_ARG_0_._tweak_data.buy_all_assets.name_id = "menu_asset_buy_all_assets"
	_ARG_0_._tweak_data.buy_all_assets.unlock_desc_id = "menu_asset_buy_all_assets_desc"
	_ARG_0_._tweak_data.buy_all_assets.visible_if_locked = true
	_ARG_0_._tweak_data.buy_all_assets.no_mystery = true
	_ARG_0_:update_buy_all_assets_asset_cost()
	for _FORV_4_, _FORV_5_ in ipairs(_ARG_0_._global.assets) do
		if _FORV_5_.id == "gage_assignment" then
			_ARG_0_._gage_saved = deep_clone(_FORV_5_)
			_FORV_5_.id = "buy_all_assets"
			_FORV_5_.unlocked = false
			_FORV_5_.can_unlock = true
			_FORV_5_.no_mystery = true
			break
		end
	end
end

function MissionAssetsManager.sync_unlock_asset(_ARG_0_, ...)
	_ARG_0_.oldMethods.sync_unlock_asset(_ARG_0_, ...)
	_ARG_0_:update_buy_all_assets_asset_cost()
	for _FORV_6_, _FORV_7_ in ipairs(_ARG_0_._global.assets) do
		if _FORV_7_.id ~= "buy_all_assets" and _FORV_7_.show and not _FORV_7_.unlocked and (Network:is_server() and _FORV_7_.can_unlock or _ARG_0_:get_asset_can_unlock_by_id(_FORV_7_.id)) then
			break
		end
	end

	if false and not _ARG_0_._all_assets_bought then
		_ARG_0_._tweak_data.buy_all_assets.money_lock = 0
		_ARG_0_._all_assets_bought = true
		_ARG_0_.oldMethods.unlock_asset(_ARG_0_, "buy_all_assets")
	end
end

function MissionAssetsManager.unlock_asset(_ARG_0_, _ARG_1_)
	if _ARG_1_ ~= "buy_all_assets" or not game_state_machine or not _ARG_0_:is_unlock_asset_allowed() then
		return _ARG_0_.oldMethods.unlock_asset(_ARG_0_, _ARG_1_)
	end

	for _FORV_5_, _FORV_6_ in ipairs(_ARG_0_._global.assets) do
		if _FORV_6_.id ~= "buy_all_assets" and _FORV_6_.show and not _FORV_6_.unlocked and (Network:is_server() and _FORV_6_.can_unlock or _ARG_0_:get_asset_can_unlock_by_id(_FORV_6_.id)) then
			_ARG_0_.oldMethods.unlock_asset(_ARG_0_, _FORV_6_.id)
		end
	end
end

function MissionAssetsManager.sync_save(_ARG_0_, _ARG_1_)
	clone(_ARG_0_._global).assets = clone(clone(_ARG_0_._global).assets)
	for _FORV_6_, _FORV_7_ in ipairs(clone(_ARG_0_._global).assets) do
		if _FORV_7_.id == "buy_all_assets" then
			clone(_ARG_0_._global).assets[_FORV_6_] = _ARG_0_._gage_saved
			break
		end
	end

	_ARG_1_.MissionAssetsManager = clone(_ARG_0_._global)
end

function MissionAssetsManager.sync_load(_ARG_0_, _ARG_1_, ...)
	_ARG_0_._global = _ARG_1_.MissionAssetsManager
	_ARG_0_:insert_buy_all_assets_asset()
	_ARG_0_.oldMethods.sync_load(_ARG_0_, _ARG_1_, ...)
end

