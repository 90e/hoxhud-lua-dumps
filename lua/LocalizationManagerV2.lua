clone_methods(LocalizationManager)
LocalizationManager.hoxhud = {}
function LocalizationManager.calculate_assault_data(_ARG_0_)
	if managers.groupai:state()._task_data.assault.phase == "build" then
	elseif managers.groupai:state()._task_data.assault.phase == "sustain" then
	end

	if managers.groupai:state()._hunt_mode then
	end

	return managers.groupai:state()._task_data.assault.phase, 0 <= (managers.groupai:state()._task_data.assault.phase == "fade" and 0 or managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.force_pool) * managers.groupai:state():_get_balancing_multiplier(tweak_data.group_ai.besiege.assault.force_pool_balance_mul) - (managers.groupai:state()._hunt_mode and 0 or managers.groupai:state()._task_data.assault.force_spawned)) and string.format("%d", managers.groupai:state()._task_data.assault.phase == "fade" and 0 or managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.force_pool) * managers.groupai:state():_get_balancing_multiplier(tweak_data.group_ai.besiege.assault.force_pool_balance_mul) - (managers.groupai:state()._hunt_mode and 0 or managers.groupai:state()._task_data.assault.force_spawned)) or 0, managers.groupai:state()._task_data.assault.phase_end_t + math.lerp(managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.sustain_duration_min), managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.sustain_duration_max), math.random()) * managers.groupai:state():_get_balancing_multiplier(tweak_data.group_ai.besiege.assault.sustain_duration_balance_mul) + tweak_data.group_ai.besiege.assault.fade_duration + tweak_data.group_ai.besiege.assault.fade_duration >= 0 and string.format("%.2f", managers.groupai:state()._task_data.assault.phase_end_t + math.lerp(managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.sustain_duration_min), managers.groupai:state():_get_difficulty_dependent_value(tweak_data.group_ai.besiege.assault.sustain_duration_max), math.random()) * managers.groupai:state():_get_balancing_multiplier(tweak_data.group_ai.besiege.assault.sustain_duration_balance_mul) + tweak_data.group_ai.besiege.assault.fade_duration + tweak_data.group_ai.besiege.assault.fade_duration + 350 - managers.groupai:state()._t) or "OVERDUE"
end

function LocalizationManager.hoxhud.hud_assault_stats(_ARG_0_)
	if managers.groupai:state():get_hunt_mode() then
		return _ARG_0_.oldMethods.text(_ARG_0_, "hud_assault_assault")
	end

	return (_ARG_0_:calculate_assault_data() and (tweak_data.hoxhud.assault_phase_text .. tweak_data.hoxhud.phase_map[_ARG_0_:calculate_assault_data()] .. " /// ") .. tweak_data.hoxhud.assault_spawn_amount_text .. _ARG_0_:calculate_assault_data() .. " /// " or tweak_data.hoxhud.assault_phase_text .. tweak_data.hoxhud.phase_map[_ARG_0_:calculate_assault_data()] .. " /// ") .. tweak_data.hoxhud.assault_time_left_text .. _ARG_0_:calculate_assault_data()
end

function LocalizationManager.text(_ARG_0_, _ARG_1_, ...)
	return _ARG_0_.hoxhud[_ARG_1_] and _ARG_0_.hoxhud[_ARG_1_](_ARG_0_) or _ARG_0_.oldMethods.text(_ARG_0_, _ARG_1_, ...)
end

getmetatable(Localizer)._lookup = getmetatable(Localizer).lookup
getmetatable(Localizer)._exists = getmetatable(Localizer).exists
getmetatable(Localizer).exists = function(_ARG_0_, _ARG_1_, ...)
	return tweak_data.hoxhud.local_strings[tostring(_ARG_1_)] ~= nil or _ARG_0_:_exists(_ARG_1_, ...)
end

getmetatable(Localizer).lookup = function(_ARG_0_, _ARG_1_, ...)
	return tweak_data.hoxhud.local_strings[tostring(_ARG_1_)] and managers.localization:_localizer_post_process(tweak_data.hoxhud.local_strings[tostring(_ARG_1_)]) or _ARG_0_:_lookup(_ARG_1_, ...)
end

