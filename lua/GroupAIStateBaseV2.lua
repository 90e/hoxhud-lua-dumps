clone_methods(GroupAIStateBase)
function GroupAIStateBase.on_hostage_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	_ARG_0_.oldMethods.on_hostage_state(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_, ...)
	if _ARG_3_ then
		managers.hud:set_control_info({
			nr_hostages = _ARG_0_:hostage_count(),
			nr_dominated = _ARG_0_:police_hostage_count()
		})
	end
end

function GroupAIStateBase.set_police_hostage_count(_ARG_0_, _ARG_1_)
	_ARG_0_._police_hostage_headcount = _ARG_1_
end

function GroupAIStateBase.convert_hostage_to_criminal(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
	return (_ARG_0_.oldMethods.convert_hostage_to_criminal(_ARG_0_, ...))
end

function GroupAIStateBase.sync_converted_enemy(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
	return (_ARG_0_.oldMethods.sync_converted_enemy(_ARG_0_, ...))
end

function GroupAIStateBase.clbk_minion_dies(_ARG_0_, ...)
	_ARG_0_.oldMethods.clbk_minion_dies(_ARG_0_, ...)
	managers.hud:set_control_info({
		nr_jokered = _ARG_0_:get_amount_enemies_converted_to_criminals() or 0
	})
end

function GroupAIStateBase.set_whisper_mode(_ARG_0_, ...)
	_ARG_0_.oldMethods.set_whisper_mode(_ARG_0_, ...)
	if (...) then
		managers.hud:set_info_box_mode("stealth")
	else
		managers.hud:set_info_box_mode("loud")
	end
end

function GroupAIStateBase.is_pacified_civilian(_ARG_0_, _ARG_1_)
	return _ARG_1_:anim_data().tied and not _ARG_1_:contour()._contour_list
end

function GroupAIStateBase._upd_criminal_suspicion_progress(_ARG_0_, ...)
	_ARG_0_.oldMethods._upd_criminal_suspicion_progress(_ARG_0_, ...)
	if not next(_ARG_0_._suspicion_hud_data) or not _ARG_0_._ai_enabled then
		return
	end

	_ARG_0_._upd_pacified_civilian_alerts(_ARG_0_._suspicion_hud_data)
end

