clone_methods(TextTemplateBase)
function TextTemplateBase._big_bank_welcome(_ARG_0_)
	if tweak_data.hoxhud.no_silly_stock_ticker then
		return _ARG_0_.oldMethods._big_bank_welcome(_ARG_0_)
	end

	_ARG_0_._unit:text_gui():set_row_speed(1, 500)
	_ARG_0_._unit:text_gui():set_row_speed(2, 240 + 120 * math.rand(1))
	_ARG_0_._unit:text_gui():set_row_gap(1, 320)
	for _FORV_4_ = 1, _ARG_0_._unit:text_gui().ROWS do
		_ARG_0_._unit:text_gui():clear_row_and_guis(_FORV_4_)
	end

	do break end
	_ARG_0_._unit:text_gui():add_text(1, "they might notice the sound", "green")
	_ARG_0_._unit:text_gui():add_text(1, "-", "green")
	_ARG_0_._unit:text_gui():add_text(1, "Welcome to the Benevolent bank", "green")
	_ARG_0_._unit:text_gui():add_text(1, "-", "green")
	do break end
	_ARG_0_._unit:text_gui():add_text(2, "they might notice the sound", "light_green")
	_ARG_0_._unit:text_gui():add_text(2, " - ", "light_green")
end

function TextTemplateBase._stock_ticker(_ARG_0_)
	if tweak_data.hoxhud.no_silly_stock_ticker then
		return _ARG_0_.oldMethods._stock_ticker(_ARG_0_)
	end

	for _FORV_4_ = 1, _ARG_0_._unit:text_gui().ROWS do
		_ARG_0_._unit:text_gui():set_row_gap(_FORV_4_, 20)
		_ARG_0_._unit:text_gui():clear_row_and_guis(_FORV_4_)
		_ARG_0_._unit:text_gui():set_row_speed(_FORV_4_, _FORV_4_ * 100 + 40 + 120 * math.rand(1))
	end

	_FOR_["Team$Evil"] = 1
	if not TextTemplateBase.STOCK_PERCENT then
		TextTemplateBase.STOCK_PERCENT = {}
		do break end
		if (_ARG_0_._unit:text_gui() ~= 2 or not math.rand(-5, -55)) and (_ARG_0_._unit:text_gui() ~= 3 or not math.rand(9001, 9999)) then
		end

		TextTemplateBase.STOCK_PERCENT[_ARG_0_._unit:text_gui().set_row_speed] = math.rand(5, 40)
	end

	do break end
	_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, _ARG_0_._unit:text_gui(), "white")
	_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, "" .. (TextTemplateBase.STOCK_PERCENT[_ARG_0_._unit:text_gui()] < 0 and "" or "+") .. string.format("%.2f", TextTemplateBase.STOCK_PERCENT[_ARG_0_._unit:text_gui()]) .. "%", TextTemplateBase.STOCK_PERCENT[_ARG_0_._unit:text_gui()] < 0 and "light_red" or "light_green", _ARG_0_._unit:text_gui().FONT_SIZE / 1.5, "bottom", nil)
	_ARG_0_._unit:text_gui():add_text(math.mod(1, _ARG_0_._unit:text_gui().ROWS) + 1, "  ", "white")
end

