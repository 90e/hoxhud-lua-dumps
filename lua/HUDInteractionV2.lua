clone_methods(HUDInteraction)
function HUDInteraction.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	_ARG_0_._interact_timer_text = _ARG_0_._hud_panel:text({
		name = "interact_timer_text",
		visible = false,
		text = "",
		valign = "center",
		align = "center",
		layer = 2,
		color = Color.white,
		font = tweak_data.menu.pd2_large_font,
		font_size = tweak_data.hud_present.text_size + 8,
		h = 64
	})
	for _FORV_7_ = 1, 4 do
		_ARG_0_["_bgtext" .. _FORV_7_] = _ARG_0_._hud_panel:text({
			name = "bgtext" .. _FORV_7_,
			visible = false,
			text = "",
			valign = "center",
			align = "center",
			layer = 1,
			color = Color.black,
			font = tweak_data.menu.pd2_large_font,
			font_size = tweak_data.hud_present.text_size + 8,
			h = 64
		})
	end

	_FOR_:set_y(_ARG_0_._hud_panel:h() / 2)
	_ARG_0_._bgtext1:set_y(_ARG_0_._hud_panel:h() / 2 - 1)
	_ARG_0_._bgtext1:set_x(_ARG_0_._bgtext1:x() - 1)
	_ARG_0_._bgtext2:set_y(_ARG_0_._hud_panel:h() / 2 + 1)
	_ARG_0_._bgtext2:set_x(_ARG_0_._bgtext2:x() + 1)
	_ARG_0_._bgtext3:set_y(_ARG_0_._hud_panel:h() / 2 + 1)
	_ARG_0_._bgtext3:set_x(_ARG_0_._bgtext3:x() - 1)
	_ARG_0_._bgtext4:set_y(_ARG_0_._hud_panel:h() / 2 - 1)
	_ARG_0_._bgtext4:set_x(_ARG_0_._bgtext4:x() + 1)
	_ARG_0_._complete_color = Color[managers.user:get_setting("hoxhud_interact_timer_color")]
end

function HUDInteraction.show_interaction_bar(_ARG_0_, ...)
	_ARG_0_.oldMethods.show_interaction_bar(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_anticheat_only") or not managers.user:get_setting("hoxhud_show_interact_timer") then
		return
	end

	_ARG_0_._interact_circle:set_visible(managers.user:get_setting("hoxhud_show_interact_circle"))
	for _FORV_5_ = 1, 4 do
		_ARG_0_["_bgtext" .. _FORV_5_]:set_visible(true)
	end

	_FOR_:set_visible(true)
end

function HUDInteraction.set_interaction_bar_width(_ARG_0_, _ARG_1_, _ARG_2_)
	_ARG_0_.oldMethods.set_interaction_bar_width(_ARG_0_, _ARG_1_, _ARG_2_)
	if not _ARG_0_._interact_timer_text then
		return
	end

	_ARG_0_._interact_timer_text:set_text(string.format("%.1f", _ARG_2_ - _ARG_1_ >= 0 and _ARG_2_ - _ARG_1_ or 0) .. "s")
	for _FORV_8_ = 1, 4 do
		_ARG_0_["_bgtext" .. _FORV_8_]:set_text(string.format("%.1f", _ARG_2_ - _ARG_1_ >= 0 and _ARG_2_ - _ARG_1_ or 0) .. "s")
	end

	_FOR_:set_color(Color(_ARG_0_._complete_color.a + (1 - _ARG_1_ / _ARG_2_), _ARG_0_._complete_color.r + (1 - _ARG_1_ / _ARG_2_), _ARG_0_._complete_color.g + (1 - _ARG_1_ / _ARG_2_), _ARG_0_._complete_color.b + (1 - _ARG_1_ / _ARG_2_)))
end

function HUDInteraction.hide_interaction_bar(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_show_interact_circle") or managers.user:get_setting("hoxhud_anticheat_only") then
		_ARG_0_.oldMethods.hide_interaction_bar(_ARG_0_, ...)
		if managers.user:get_setting("hoxhud_anticheat_only") or not managers.user:get_setting("hoxhud_show_interact_timer") then
			return
		end
	end

	_ARG_0_._interact_timer_text:set_visible(false)
	for _FORV_5_ = 1, 4 do
		_ARG_0_["_bgtext" .. _FORV_5_]:set_visible(false)
	end
end

function HUDInteraction.destroy(_ARG_0_, ...)
	_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("interact_timer_text"))
	_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("bgtext1"))
	_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("bgtext2"))
	_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("bgtext3"))
	_ARG_0_._hud_panel:remove(_ARG_0_._hud_panel:child("bgtext4"))
	_ARG_0_.oldMethods.destroy(_ARG_0_, ...)
end

