clone_methods(MissionBriefingGui)
function MissionBriefingGui.init(_ARG_0_, ...)
	_ARG_0_.oldMethods.init(_ARG_0_, ...)
	_ARG_0_._ready_up_counter = 0
end

function MissionBriefingGui.on_ready_pressed(_ARG_0_, ...)
	_ARG_0_.oldMethods.on_ready_pressed(_ARG_0_, ...)
	if Network:is_server() and tweak_data.hoxhud.debug_enable_force_ready then
		for _FORV_5_, _FORV_6_ in pairs(managers.network:session():peers()) do
			if not _FORV_6_:synched() then
				return
			end
		end

		_ARG_0_._ready_up_counter = _ARG_0_._ready_up_counter + 1
		if _ARG_0_._ready_up_counter > tweak_data.hoxhud.force_ready_threshold then
			managers.network:game():spawn_players()
		end
	end
end

