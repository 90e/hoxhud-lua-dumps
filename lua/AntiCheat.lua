if not SimpleMenu then
	SimpleMenu = class()
	function SimpleMenu.init(_ARG_0_, _ARG_1_, _ARG_2_, _ARG_3_)
		_ARG_0_.dialog_data = {
			title = _ARG_1_,
			text = _ARG_2_,
			button_list = {},
			id = tostring(math.random(0, 4.2949673E9))
		}
		
		_ARG_0_.visible = false
		for _FORV_7_, _FORV_8_ in ipairs(_ARG_3_) do
			_FORV_8_.data = _FORV_8_.data or nil
			_FORV_8_.callback = _FORV_8_.callback or nil
			if _FORV_8_.is_focused_button then
				_ARG_0_.dialog_data.focus_button = #_ARG_0_.dialog_data.button_list + 1
			end

			table.insert(_ARG_0_.dialog_data.button_list, {
				text = _FORV_8_.text,
				callback_func = callback(_ARG_0_, _ARG_0_, "_do_callback", {
					data = _FORV_8_.data,
					callback = _FORV_8_.callback
				}),
				cancel_button = _FORV_8_.is_cancel_button or false
			})
		end

		return _ARG_0_
	end

	function SimpleMenu._do_callback(_ARG_0_, _ARG_1_)
		if _ARG_1_.callback then
			if _ARG_1_.data then
				_ARG_1_.callback(_ARG_1_.data)
			else
				_ARG_1_.callback()
			end
		end

		_ARG_0_.visible = false
	end

	function SimpleMenu.show(_ARG_0_)
		if _ARG_0_.visible then
			return
		end

		_ARG_0_.visible = true
		managers.system_menu:show(_ARG_0_.dialog_data)
	end

	function SimpleMenu.hide(_ARG_0_)
		if _ARG_0_.visible then
			managers.system_menu:close(_ARG_0_.dialog_data.id)
			_ARG_0_.visible = false
			return
		end
	end
end

if RequiredScript == "lib/utils/game_state_machine/gamestatemachine" then
	clone_methods(GameStateMachine)
	function GameStateMachine.check_money_abuse(_ARG_0_)
		if not Global.oldOffshore or not Global.oldSpending or not Global.oldCollected then
			Global.oldOffshore = managers.money:offshore()
			Global.oldSpending = managers.money:total()
			Global.oldCollected = managers.money:total_collected()
			return
		end

		if managers.money:total() - Global.oldSpending > tweak_data.hoxhud.money_cheater_threshold then
			_ARG_0_._simpleMenu = SimpleMenu:new(tweak_data.hoxhud.money_cheated_dialog_title, string.format(tweak_data.hoxhud.money_cheated_dialog_text, managers.money:add_decimal_marks_to_string("" .. Global.oldSpending), managers.money:add_decimal_marks_to_string("" .. Global.oldOffshore)), {
				{
					text = tweak_data.hoxhud.dialog_yes,
					callback = _ARG_0_.fix_my_cash,
					data = _ARG_0_
				},
				{
					text = tweak_data.hoxhud.dialog_no,
					callback = _ARG_0_.keeping_the_money,
					data = _ARG_0_
				}
			})
			_ARG_0_._simpleMenu:show()
		elseif managers.money:total() > Global.oldSpending then
			_ARG_0_:keeping_the_money()
		end
	end

	function GameStateMachine.fix_my_cash(_ARG_0_)
		managers.money:_set_total(Global.oldSpending)
		managers.money:_set_offshore(Global.oldOffshore)
		managers.money:_set_total_collected(Global.oldCollected)
		_ARG_0_:keeping_the_money()
	end

	function GameStateMachine.keeping_the_money(_ARG_0_)
		Global.oldSpending = managers.money:total()
		Global.oldOffshore = managers.money:offshore()
		Global.oldCollected = managers.money:total_collected()
	end

	function GameStateMachine.change_state_by_name(_ARG_0_, _ARG_1_, ...)
		if _ARG_1_ == "ingame_lobby" or _ARG_1_ == "menu_main" then
			Global.needMoneyCheck = true
		end

		return _ARG_0_.oldMethods.change_state_by_name(_ARG_0_, _ARG_1_, ...)
	end
end

