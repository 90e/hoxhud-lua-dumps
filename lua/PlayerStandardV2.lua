clone_methods(PlayerStandard)
function PlayerStandard._update_fwd_ray(_ARG_0_)
	_ARG_0_.oldMethods._update_fwd_ray(_ARG_0_)
	if game_state_machine:current_state_name() == "ingame_waiting_for_respawn" then
		managers.hud:set_enemy_health_visible(false)
		return
	end

	if not _ARG_0_._fwd_ray or not alive(_ARG_0_._fwd_ray.unit) or not managers.user:get_setting("hoxhud_show_civilian_health") and managers.enemy:is_civilian(_ARG_0_._fwd_ray.unit) then
		return
	end

	if _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage() then
		if _ARG_0_._no_char_last_tick and type(_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health) == "number" and _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health <= 0 then
			return
		end

		_ARG_0_._last_char = _ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit
		managers.hud:set_enemy_health({
			current = (_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._health or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1),
			total = (_ARG_0_._fwd_ray and alive(_ARG_0_._fwd_ray.unit) and _ARG_0_._fwd_ray.unit:character_damage()._HEALTH_INIT or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1)
		})
		managers.hud:set_enemy_health_visible(true)
		_ARG_0_._no_char_last_tick = false
	else
		if alive(_ARG_0_._last_char) then
			managers.hud:set_enemy_health({
				current = (_ARG_0_._last_char:character_damage()._health or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1),
				total = (_ARG_0_._last_char:character_damage()._HEALTH_INIT or 0) * (tweak_data.hoxhud.show_multiplied_enemy_health and 10 or 1)
			})
		end

		managers.hud:set_enemy_health_visible(false)
		_ARG_0_._no_char_last_tick = true
	end
end

function PlayerStandard._start_action_throw_grenade(_ARG_0_, ...)
	tweak_data.hoxhud.allow_grenades_in_stealth = true
	return _ARG_0_.oldMethods._start_action_throw_grenade(_ARG_0_, ...)
end

function PlayerStandard._start_action_reload(_ARG_0_, _ARG_1_, ...)
	_ARG_0_.oldMethods._start_action_reload(_ARG_0_, _ARG_1_, ...)
	if _ARG_0_._equipped_unit:base():can_reload() and managers.user:get_setting("hoxhud_show_timer_on_reload") and not managers.user:get_setting("hoxhud_anticheat_only") and managers.player:current_state() ~= "bleed_out" then
		_ARG_0_._state_data._isReloading = true
		managers.hud:show_interaction_bar(0, _ARG_0_._state_data.reload_expire_t or 0)
		_ARG_0_._state_data.reload_offset = _ARG_1_
	end
end

function PlayerStandard._update_reload_timers(_ARG_0_, _ARG_1_, ...)
	_ARG_0_.oldMethods._update_reload_timers(_ARG_0_, _ARG_1_, ...)
	if not managers.user:get_setting("hoxhud_show_timer_on_reload") or managers.user:get_setting("hoxhud_anticheat_only") then
		return
	elseif not _ARG_0_._state_data.reload_expire_t and _ARG_0_._state_data._isReloading then
		managers.hud:hide_interaction_bar(true)
		_ARG_0_._state_data._isReloading = false
	elseif _ARG_0_._state_data._isReloading and managers.player:current_state() ~= "bleed_out" then
		managers.hud:set_interaction_bar_width(_ARG_1_ and _ARG_1_ - _ARG_0_._state_data.reload_offset or 0, _ARG_0_._state_data.reload_expire_t and _ARG_0_._state_data.reload_expire_t - _ARG_0_._state_data.reload_offset or 0)
	end
end

function PlayerStandard._interupt_action_reload(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_show_timer_on_reload") and not managers.user:get_setting("hoxhud_anticheat_only") and managers.player:current_state() ~= "bleed_out" then
		managers.hud:hide_interaction_bar(false)
		_ARG_0_._state_data._isReloading = false
	end

	return _ARG_0_.oldMethods._interupt_action_reload(_ARG_0_, ...)
end

function PlayerStandard._stance_entered(_ARG_0_, ...)
	if managers.user:get_setting("hoxhud_rotate_for_angled_sight") then
		_ARG_0_._camera_unit:base():set_want_rotated(not _ARG_0_._state_data.in_steelsight and _ARG_0_._equipped_unit:base():is_second_sight_on() and not _ARG_0_:_is_reloading())
		_ARG_0_._camera_unit:base():set_want_restored(not _ARG_0_._state_data.in_steelsight and (not _ARG_0_._equipped_unit:base():is_second_sight_on() or _ARG_0_:_is_reloading()))
		_ARG_0_._camera_unit:base():set_weapon_name(_ARG_0_._equipped_unit:base()._name_id)
	end

	return _ARG_0_.oldMethods._stance_entered(_ARG_0_, ...)
end

function PlayerStandard._get_input(_ARG_0_, ...)
	if managers.player:should_do_press_to_interact() then
		_ARG_0_.oldMethods._get_input(_ARG_0_, ...).btn_interact_release = _ARG_0_._controller:get_input_pressed("use_item")
		return (_ARG_0_.oldMethods._get_input(_ARG_0_, ...))
	else
		return _ARG_0_.oldMethods._get_input(_ARG_0_, ...)
	end
end

