These dumps are taken from HoxHud P4.1


./luac/ contains the raw dumped compiled Lua.

./lua/ contains the decompiled Lua.

./standalone/ contains files to use HoxHud without its Windows binaries (IPHLPAPI/PD2API1)


All files use UNIX style line breaks (\n)


Credits:

-90e    - Dumping and decompiling the Lua, making the setup

-gir489 - Defeating TheMida

-B1313 - Wildcard info
